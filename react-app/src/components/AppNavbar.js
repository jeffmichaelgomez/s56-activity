import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import logo from '../images/logo.png'
import { Container } from 'react-bootstrap';
import { Fragment,useContext } from 'react';
import UserContext from '../UserContext';

// AppNavbar component
export default function AppNavbar(){
	const { user } = useContext(UserContext);

	return(
		<div style={{background: "#1c2f54"}}>
		<Navbar expand="lg" variant="dark">
		<Container fluid>
		    <Navbar.Brand as={NavLink} to="/" exact>
		      <img
		        alt=""
		        src={logo}
		        width="100"
		        height="100"
		        className="d-inline-block align-top"
		      />{' '}
		    </Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" className="m-3" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="justify-content-end flex-grow-1 pe-3">
		        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/" exact>Help</Nav.Link>
		        {(user.id !== null) ? 
		        	<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		        	:
		        	<Fragment>
		        		<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		        		<Nav.Link as={NavLink} to="/register" exact>Signup</Nav.Link>
		        	</Fragment>
		        }
		      </Nav>
		    </Navbar.Collapse>
		    </Container>
		</Navbar>
		</div>
	)
}
/*
- The "as" prop allows components to be treated as if they are a different component gaining access to it's properties and functionalities.
- The "to" prop is used in place of the "href" prop for providing the URL for the page.
- The "exact" prop is used to highlight the active NavLink component that matches the URL.
*/
