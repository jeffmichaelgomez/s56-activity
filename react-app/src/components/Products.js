import {Row,Col,Card,Button} from 'react-bootstrap';
import productList from '../data/productList'
import { Container } from 'react-bootstrap';
import { useState, useEffect} from 'react';
import Swal from 'sweetalert2'


export default function Products() {
function addToCart(e){
	e.preventDefault();
		Swal.fire({
		    title:"Successfully Added",
		    icon:"success",
		    text:"Happy Shopping!"
		});
}
	return (
	<Container>
	<Row>
	{productList.map((card, k) => (
			<Col key={k} xs={12} md={6} lg={3} className="mb-3">
			<Card style={{ width: '18rem' }}>
			  <Card.Img variant="top" src={card.image} />
			  <Card.Body>
			    <Card.Title>{card.productName}</Card.Title>
			    <Card.Text>
			      {card.description}
			    </Card.Text>
			    <Card.Text>
			      Php {card.price}
			    </Card.Text>
			    <Button variant="danger" type="submit" onClick={(e) => addToCart(e)}>Add to Cart</Button>
			  </Card.Body>
			</Card>
			</Col>
		))}
			</Row>
			</Container>
	)
}
