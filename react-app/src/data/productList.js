

const productList = [
{
	productId:1,
	productName:"Keyboard A",
	description:"Description of Keyboard A",
	price:1000,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/RO692415-a_800x.jpg?v=1636619363"
},
{
	productId:2,
	productName:"Mouse A",
	description:"Description of Mouse A",
	price:999,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/KIHX2285-a_800x.jpg?v=1632398709"
},
{
	productId:3,
	productName:"Headset A",
	description:"Description of Headset A",
	price:888,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/COCS2344-a_800x.jpg?v=1634192938"
},
{
	productId:4,
	productName:"Motherboard A",
	description:"Description of Motherboard A",
	price:777,
	category:"Components",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/AsusPrimeA520M-KAMDA520_RyzenAM4_withM.2support_1GbEthernet_HDMID-Sub_SATA6Gbps_USB3.2Gen1Type-A_microATXmotherboard-a_800x.jpg?v=1618907273"
},
{
	productId:5,
	productName:"Keyboard B",
	description:"Description of Keyboard A",
	price:1000,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/RO692415-a_800x.jpg?v=1636619363"
},
{
	productId:6,
	productName:"Mouse B",
	description:"Description of Mouse A",
	price:999,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/KIHX2285-a_800x.jpg?v=1632398709"
},
{
	productId:7,
	productName:"Headset B",
	description:"Description of Headset A",
	price:888,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/COCS2344-a_800x.jpg?v=1634192938"
},
{
	productId:8,
	productName:"Motherboard B",
	description:"Description of Motherboard A",
	price:777,
	category:"Components",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/AsusPrimeA520M-KAMDA520_RyzenAM4_withM.2support_1GbEthernet_HDMID-Sub_SATA6Gbps_USB3.2Gen1Type-A_microATXmotherboard-a_800x.jpg?v=1618907273"
},
{
	productId:9,
	productName:"Keyboard C",
	description:"Description of Keyboard A",
	price:1000,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/RO692415-a_800x.jpg?v=1636619363"
},
{
	productId:10,
	productName:"Mouse C",
	description:"Description of Mouse A",
	price:999,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/KIHX2285-a_800x.jpg?v=1632398709"
},
{
	productId:11,
	productName:"Headset C",
	description:"Description of Headset A",
	price:888,
	category:"Peripherals",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/COCS2344-a_800x.jpg?v=1634192938"
},
{
	productId:12,
	productName:"Motherboard C",
	description:"Description of Motherboard A",
	price:777,
	category:"Components",
	isActive:true,
	image:"https://cdn.shopify.com/s/files/1/0101/4864/2879/products/AsusPrimeA520M-KAMDA520_RyzenAM4_withM.2support_1GbEthernet_HDMID-Sub_SATA6Gbps_USB3.2Gen1Type-A_microATXmotherboard-a_800x.jpg?v=1618907273"
}
]

export default productList;