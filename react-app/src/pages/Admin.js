import {Container,Card,Col,Row,Button,Form,FormControl} from 'react-bootstrap';
import { useState,useEffect,useContext } from 'react';
import Swal from 'sweetalert2'
import { useHistory } from 'react-router-dom';
import { Link,NavLink } from 'react-router-dom';



export default function	Admin(){

	return(
		<Container className="mt-5">
		<h1 className="mb-3">Admin Page</h1>
		<Row>
		<Link>Add Product</Link>
		</Row>
		<Row>
		<Link>View Messages</Link>
		</Row>
		<Row>
		<Form className="d-flex justify-content-start mb-3 mt-3">
		<Button variant="outline-success" className="me-3">Search</Button>
		<Col md={4}>
		  <FormControl
		    type="search"
		    placeholder="Search for Product"
		    className="me-2 mb-1"
		    aria-label="Search"
		  />
		</Col>
		</Form>
		<Form className="d-flex justify-content-start mb-5">
		<Button variant="outline-success" className="me-3">Search</Button>
		<Col md={4}>
		  <FormControl
		    type="search"
		    placeholder="Search for User"
		    className="me-2 mb-1"
		    aria-label="Search"
		  />
		</Col>
		</Form>
		</Row>
		<Form>
		<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
		  <Form.Label>Result</Form.Label>
		  <Form.Control as="textarea" rows={6}/>
		</Form.Group>
		</Form>

		</Container>

	)

}