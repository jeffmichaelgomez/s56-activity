import { Fragment } from 'react'
import Banner from '../components/Banner';
import Products from '../components/Products';

export default function	Home(){
	return(
		<Fragment>
			<h1>HOME</h1>
			<Products/>
		</Fragment>
	)
}