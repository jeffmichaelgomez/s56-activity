import {Container,Card,Col,Row,Button,Form} from 'react-bootstrap';
import { useState,useEffect,useContext } from 'react';
import Swal from 'sweetalert2'
import CardGroup from 'react-bootstrap/CardGroup'
import { useHistory } from 'react-router-dom';
import facebook from '../images/facebook.png'
import google from '../images/google.png'
import { NavLink } from 'react-router-dom';

export default function	Login(){

const [email, setEmail] = useState('');
const [password1, setPassword1] = useState('');
const [isActive, setIsActive] = useState(false);

let history = useHistory();

function registerUser(e){
	e.preventDefault();

	setEmail('');
	setPassword1('');

	Swal.fire({
	    title:"Login Successful",
	    icon:"success",
	    text:"Happy Shopping!"
	});
	history.push('/')
}

useEffect(() => {
  if(email !== '' &&password1 !== '')
  {
    setIsActive(true);
  }
  else{
    setIsActive(false);
  }
}, [email,password1])

	return(
		<Container fluid="lg">
		<Row lg={2}>
		<Card>
			<Col className="w-25 h-100 p-3 d-none d-lg-block">
				<img src="https://cf.shopee.ph/file/19824fb3d9ef94b3bb4a163a666ac94f"/>
			</Col>
		</Card>
		<Card>
		<Form onSubmit={(e) => registerUser(e)}>
			<Col>
			<Form.Label><h2>Login</h2></Form.Label>
			</Col>
			<Col>
				  <Form.Group className="mb-1" controlId="userEmail" xs={12} md={6} lg={4}>
				    <Form.Label>Email address</Form.Label>
				    <Form.Control
				    column lg="2" 
				    type="email" 
				    placeholder="Enter 
				    Email" value={email}
				    onChange = {e => setEmail(e.target.value)}
				    required />
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				  </Form.Group>
			</Col>
			<Col className="mb-3">
				  <Form.Group className="mb-1" controlId="password1">
				    <Form.Label>Password</Form.Label>
				    <Form.Control
				    column lg="2"
				    type="password" 
				    placeholder="Password" 
				    value={password1}
				    onChange={e => setPassword1(e.target.value)}
					required />
				  </Form.Group>
			</Col>
			<div>
			<Form.Label as={NavLink} to="/" exact className="d-flex justify-content-end">Forgot Password?</Form.Label>
			<Form.Label className="mb-3">Login with</Form.Label>
			</div>
			<Col>
			<div className="d-flex justify-content-start mb-3 mt-0 pt-0">
			<a href='https://google.com' target="_blank"><img src={google} alt="google"className="logo"/></a>
			<a href='https://facebook.com' target="_blank"><img src={facebook} alt="facebook" className="logo"/></a>
			</div>
			<div className="d-flex justify-content-end mb-3">
				  { isActive ?
				          <Button variant="primary" type="submit" id="submitBtn">
				            Submit
				          </Button>
				          :
				          <Button variant="danger" type="submit" id="submitBtn" disabled>
				            Submit
				          </Button>
				        }
			</div>
			</Col>
		</Form>
		</Card>
		</Row>
		</Container>
	)
}